# Linktree Challenge - Gareth Jones

This document herein contains the details for a submission for Linktree's Front End Technical Challenge. 

This codebase was composed in June of 2021 by Gareth Jones for an application for the role of Technical Lead.

## Getting Started 

This codebase can be spun up locally with the following command: 

```
netlify dev
```

Access to the repository can be requested [here](https://gitlab.com/gazrageous/linktree-challenge/)

You can view the project hosted [here](https://gjones-linktree-challenge.netlify.app/)

You can interact with the developed components against the brief, as well as open the PoC Tray to add new links, and change the basic theme of the links themselves. 

You can also navigate to different paths in the application, which is meant to, in a very basic form, simulate the foundation for requesting unique profile information.

## Selected Stack & Justification
---
### React:
As per brief, the use of either React or Vue as a key piece of this codebase was outlined as highly desirable. Given my more intimate familiarity with React, the utility of Hooks and Context in solving for some of the outlined acceptance criteria, and prior knowledge of it's presence in the LinkTree stack, I opted for this framework over an opportunity to utilise Vue for the same outcomes.

### TypeScript:
Given the outlined interest in Testing for this codebase, TypeScript seemed a natural choice given its ability to contribute significantly to standard JS testing practice through its prescription of strong types (when compared with common type assertion tests written in Chai for example). Paired with ESLINT, these tools enforce standards and checks at the earliest stages of app creation - at development. Similar to the above, prior knowledge of LinkTree's heavy investment in TypeScript meant this was essential for me to try and include wherever possible. 

### Jest:
Shipped as a standard with CRA, Jest seemed liked the obvious choice to use as a means of writing tests for this codebase for key components.

### Netlify Serverless Functions:
Since it was disclosed that Linktree is leveraging AWS architecture, I decided to use Netlify's serverless functions since they are an abstraction of AWS Lambda functions for this exercise. Although they are not liasing with any genuine APIs these would form the foundation of any further functionality in this space. 


## Future Development And Reflection
---

At present, the overall codebase submitted meets the base requirements of the provided brief. In the interest of time and progressing with my application, I have opted to not continue developing the features contained herein. However, given further development runway, I believe there are several areas of this application and its associated architecture that I would improve or refactor if given the opportunity:

1. ### Theming & Styles
The delivery of styles in this codebase at present is the area of the application that I am least happy with. While I would stick with the use of a Theme Context to deliver styles to components that need to subscribe to this feature, given more time I would likely fully Lean into JSS as a solution. This is cleaner in the DOM when paired with GlobalStyles and classNames and is likely more in line with Production code at LinkTree. However in the interest of time,  I used stock CSS due to the low volume of styling requirements. This was often an area of confusion against the brief, as I was unsure if the themeing was determined by the profile owner (in which case it would be set and delivered through the serverless call) or by the visiting user. These two frames of mind made it difficult to decide on a clear mode of offering this utility. I believe the context module with input fields could be repurposed either way in a settings page / tab - either exposed to all users or just the profile owner in relation to their own linktree page.


2. ### Link Componentry 
Similarly, an area I would focus on primarily is the current layout of the Links in the component tree. I believe that generally, DRY Principles not optimally observed in their current structure, due to subtle nuance between their unique requirements but overall similar visuals and structure. This decision was made from a perspective of reducing complexity for other users, in so far that it was made clear their concerns are seperated and if they were to be loaded into a component showcase (i.e. StoryBook) their expected behaviour and unique characteristics would be clearer. However, if we better breakdown common foundations between all of the unique cases, we can better optimise the component structure without a single, complex `Link` component, which is what I initially tried to avoid. Likewise, this could also help reduce the complexity and size of the provided data structure, allowing for a more homogenous array of objects that account for all the links on one's linktree page. 

3. ### TypeScript
With more time, I would better revise and more thoroughly employ TypeScript across the application to get the most value in development. Similarly, if the application were to grow any larger, I would consider moving Interface definitons to a central location. 

4. ### Serverless Functionality
While deliberatley restrictied by the parameters of the brief, the Serverless functions would be the key to developing authentic business logic in the future. This would also require the development of the reducer in this piece which would operate the CRUD functionality for the profile owner to manage their displayed links, and perhaps other information as well. 

5. ### Test Suites
As above with TypeScript, The application could do with better and more varied test coverage. 