// @todo this serverless function should eventually be connected with a genuine DB endpoint as right now it returns dummy data.
// should also paired with the LinkReducer to effectively interface for CRUD ops on links

const { v4: uuidv4 } = require('uuid');

exports.handler = async function (event) {
    const { username } = event.queryStringParameters

    return {
        statusCode: 200,
        body: `{
            "profile": {
                "userData": {
                    "name": "${!username ? "yourname" : username}",
                    "id": "${uuidv4()}"
                },
                "links": [
                    {
                        "title":"Test Link A",
                        "type": "classic_link",
                        "url":"https://www.fillmurray.com/"
                    },
                    {
                        "title":"Test Link B",
                        "type": "classic_link",
                        "url":"https://www.placecage.com/"
                    },
                    {
                        "title":"Upcoming Shows",
                        "type": "link_with_children",
                        "url":"https://www.placecage.com/",
                        "childLinks":[
                            {
                                "title":"Apr 01 2019",
                                "subtitle":"The Forum, Melbourne",
                                "url":"https://www.songkick.com/"
                            },
                            {
                                "title":"Apr 02 2019",
                                "subtitle":"Venue Name, Canberra",
                                "url":"https://www.songkick.com/",
                                "disabled":true,
                                "disabledLabel":"Sold out"
                            },
                            {
                                "title":"Apr 03 2019",
                                "subtitle":"Venue Name, Sydney",
                                "url":"https://www.songkick.com/"
                            },
                            {
                                "title":"Apr 04 2019",
                                "subtitle":"Venue Name, Brisbane",
                                "url":"https://www.songkick.com/"
                            }
                        ]
                    },
                    {
                        "title":"Your Song",
                        "type": "audio_link",
                        "url":"https://www.placecage.com/",
                        "audioChildLinks":[
                            {
                                "title":"Spotify",
                                "provider":"spotify",
                                "url":"https://www.spotify.com/au/"
                            },
                            {
                                "title":"Apple Music",
                                "provider":"apple-music",
                                "url":"https://www.apple.com/au/apple-music/"
                            },
                            {
                                "title":"Soundcloud",
                                "provider":"soundcloud",
                                "url":"https://soundcloud.com/"
                            },
                            {
                                "title":"YouTube Music",
                                "provider":"youtube",
                                "url":"https://music.youtube.com/"
                            },
                            {
                                "title":"Deezer",
                                "provider":"deezer",
                                "url":"https://www.deezer.com/en/"
                            },
                            {
                                "title":"Tidal",
                                "provider":"tidal",
                                "url":"https://tidal.com/"
                            },
                            {
                                "title":"Bandcamp",
                                "provider":"bandcamp",
                                "url":"https://bandcamp.com/"
                            }
                        ]
                    }
                ]
            }
        }`
    }
}