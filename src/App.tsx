import React from 'react';

import ThemeContextProvider from './contexts/ThemeContext';
import ProfileContextProvider from './contexts/ProfileContext';

import Header from './components/Header/Header'
import LinkList from './components/Links/LinkList'
import TestWidget from './components/TestWidget/TestWidget'
import Footer from './components/Footer/Footer'


const App: React.FC = () => {
    return (
        <div className="App">
            <ProfileContextProvider>
                <ThemeContextProvider>
                    <main>
                        <Header />
                        <LinkList />
                        <Footer />
                    </main>
                    <TestWidget />
                </ThemeContextProvider>
            </ProfileContextProvider>
        </div>
    );
}

export default App;
