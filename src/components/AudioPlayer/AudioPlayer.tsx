/*  @todo this component is not set up to operate with appropriate APIs, embeds, widgets etc. for the appropraite audio vendors, 
    however it does pull in an appropriate flag off which to piggy back to build out 
    appropriate implementations / integrations of platforms as required */

import React, { useContext } from 'react'
import { ThemeContext } from '../../contexts/ThemeContext'
import AlbumArt from '../../assets/album-art-1_1.png'
import { ReactComponent as PlayIcon } from '../../assets/icons/play.svg';
import './AudioPlayer.css'
interface AudioPlayerProps {
    vendor: string
}

const AudioPlayer: React.FC<AudioPlayerProps> = (props) => {

    const { vendor } = props
    const { theme }: any = useContext(ThemeContext)

    return (
        <div className="audioplayer">
            <img src={ AlbumArt } alt="Album art for your song"></img>
            <PlayIcon height="30" width="30" fill={ theme.colorPrimary }/>
            <div className="audioplayer-details">
                <h4>Your Song Name</h4>
                <p>Powered by: { vendor }</p>
            </div>
        </div>
    )
}

export default AudioPlayer