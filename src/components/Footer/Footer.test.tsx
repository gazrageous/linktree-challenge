import renderer from 'react-test-renderer';
import Footer from './Footer';

test('renders linktree logo', () => {
    const tree = renderer
        .create(<Footer />)
        .toJSON();
    expect(tree).toMatchSnapshot();
});
