import React from 'react'
import Logo from '../../assets/logo.svg'

const TestWidget: React.FC = () => {
    return (
        <footer>
            <img src={Logo} alt="The Linktree Logo" />
        </footer>
    )
}

export default TestWidget