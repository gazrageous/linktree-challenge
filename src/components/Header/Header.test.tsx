import renderer from 'react-test-renderer';
import {render, screen} from '@testing-library/react'
import ProfileContextProvider from '../../contexts/ProfileContext';
import Header from './Header';

test('renders header with provided profile provider / consumer', () => {
    const tree = renderer
        .create(
            <ProfileContextProvider>
                <Header />
            </ProfileContextProvider>
            )
        .toJSON();
    expect(tree).toMatchSnapshot();
});

test('username should not be blank', () => {
    render(
        <ProfileContextProvider>
            <Header />
        </ProfileContextProvider>
    )
    expect(screen.getByTestId('profile-username').innerHTML.length).not.toBeLessThanOrEqual(1)
})