import React, { useContext } from 'react'
import ProfileImage from '../../assets/profile-picture.png'
import { ProfileContext } from '../../contexts/ProfileContext';

const Header: React.FC = () => {

    const { profile }: any = useContext(ProfileContext);

    const imageAltStr = "Profile image for user: " + profile.name

    return (
        <header>
            <img src={ProfileImage} alt={imageAltStr} />
            <h1 data-testid="profile-username">@{profile.name}</h1>
        </header>
    )
}

export default Header