import renderer from 'react-test-renderer';
import { render, screen, fireEvent } from '@testing-library/react'
import ThemeContextProvider from '../../contexts/ThemeContext';
import Link from './Link';

test('renders link with theme provider', () => {
    const tree = renderer
        .create(
            <ThemeContextProvider>
                <Link title="test" url="https://jestjs.io/" />
            </ThemeContextProvider>
        )
        .toJSON();
    expect(tree).toMatchSnapshot();
});


test('colors invert on hover', () => {

    const convertToRGB = (hexCode) => {
        const aRgbHex = hexCode
            .replace("#", "")
            .match(/.{1,2}/g);
        const aRgb = [
            parseInt(aRgbHex[0], 16),
            parseInt(aRgbHex[1], 16),
            parseInt(aRgbHex[2], 16)
        ];
        return `rgb(${aRgb[0]}, ${aRgb[1]}, ${aRgb[2]})`;
    }

    const COLOR_PRIMARY = convertToRGB("#39E09B")
    const COLOR_SECONDARY = convertToRGB("#263238")

    const utils = render(
        <ThemeContextProvider>
            <Link title="test" url="https://jestjs.io/" />
        </ThemeContextProvider>
    )
    const link = utils.getByText('test')

    expect(link.style.getPropertyValue('background')).toEqual(COLOR_PRIMARY)
    expect(link.style.getPropertyValue('color')).toEqual(COLOR_SECONDARY)
    fireEvent.mouseOver(link)
    expect(link.style.getPropertyValue('color')).toEqual(COLOR_PRIMARY)
    expect(link.style.getPropertyValue('background')).toEqual(COLOR_SECONDARY) 
});



