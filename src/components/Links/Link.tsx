import React, { useContext, useState } from 'react'
import { ThemeContext } from '../../contexts/ThemeContext'

interface LinkProps {
    title: string,
    url: string
}

const Link: React.FC<LinkProps> = (props) => {

    const { title, url } = props;
    const { theme }: any = useContext(ThemeContext)

    const [inverted, setInverted] = useState(false)

    return (
        <a
            href={url}
            className="link"
            target="_blank"
            rel="noopener noreferrer"
            onMouseEnter={() => setInverted(true)}
            onMouseLeave={() => setInverted(false)}
            style={inverted ?
                {
                    background: theme.colorSecondary,
                    color: theme.colorPrimary,
                    border: `2px solid ${theme.colorPrimary}`
                }
                :
                {
                    background: theme.colorPrimary,
                    color: theme.colorSecondary,
                    border: `2px solid ${theme.colorPrimary}`
                }}>
            {title}
        </a>
    )
}

export default Link