// @todo add link to form to submit links to default template JSX on line 17

import React, { useContext } from 'react';
import Link from './Link';
import LinkWithChildLinks from './LinkWithChildLinks';
import LinkWithAudioPlayer from './LinkWithAudioPlayer';
import { ProfileContext } from '../../contexts/ProfileContext';
import './Link.css'

interface ChildLink {
    title: string,
    subtitle?: string,
    url: string,
    disabled?: boolean,
    disabledLabel?: string
}

interface AudioChildLink {
    title: string,
    provider: string,
    url: string,
}

interface ILink {
    title: string,
    type: 'audio_link' | 'classic_link' | 'link_with_children'
    url: string
    childLinks?: ChildLink[]
    audioChildLinks?: AudioChildLink[]
}

const LinkList: React.FC = () => {
    const { links }: any = useContext(ProfileContext);

    return links.length ? (
        <ul className="link-list">
            {
                links.map((link: ILink, i: number) => {
                    switch (link.type) {
                    case 'audio_link':
                        if (link.audioChildLinks) return (<LinkWithAudioPlayer key={i} title={link.title} audioChildLinks={link.audioChildLinks} />);
                        break;
                    case 'link_with_children':
                        if (link.childLinks) return (<LinkWithChildLinks key={i} title={link.title} childLinks={link.childLinks} />);
                        break;
                    default:
                        return (<Link key={i} title={link.title} url={link.url} />);
                    }
                })
            }
        </ul>
    ) : (
        <div className="empty">No links listed yet. Add some here!</div>
    );
}

export default LinkList;