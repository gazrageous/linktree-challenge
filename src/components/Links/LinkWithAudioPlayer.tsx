import React, { useContext, useState, useEffect } from 'react'
import { ThemeContext } from '../../contexts/ThemeContext'
import AudioPlayer from '../AudioPlayer/AudioPlayer';
import Arrow from '../../assets/icons/arrow.svg'

const fetchLocalAsset = (provider) => import(`../../assets/icons/${provider}.svg`);

interface AudioChildLinkProps {
    title: string,
    provider: string,
    url: string
}

interface LinkWithAudioPlayerProps {
    title: string,
    audioChildLinks: AudioChildLinkProps[]
}


const AudioChildLink = (props) => {
    const [providerLogo, setProviderLogo] = useState('')
    const { title, url, provider } = props.data
    const {  setCurrentProvider } = props

    useEffect(() => {
        fetchLocalAsset(provider).then(res => setProviderLogo(res.default))
    }, [])

    return (
        <li className="childlink audiochildlink" onClick={() => setCurrentProvider(provider)}>
            <div className="audiochildlink-logo">
                <a
                    href={url}
                    target="_blank"
                    rel="noopener noreferrer"
                >
                    <img src={providerLogo} alt={ `${provider} logo`} />
                </a>
                <h4>{title}</h4>
            </div>
            <img className="arrow" src={Arrow} alt="link arrow" />
        </li>
    )
}

const LinkWithAudioPlayer: React.FC<LinkWithAudioPlayerProps> = (props) => {

    const { title, audioChildLinks } = props;
    const { theme }: any = useContext(ThemeContext)

    const [inverted, setInverted] = useState(false)
    const [expanded, setExpanded] = useState(false)
    const [currentProvider, setCurrentProvider] = useState('')

    useEffect(() => {
        audioChildLinks.length && setCurrentProvider(audioChildLinks[0].provider)
    }, [])

    return (
        <>
            <button
                className="link link-btn"
                onMouseEnter={() => setInverted(true)}
                onMouseLeave={() => setInverted(false)}
                onClick={() => setExpanded(!expanded)}
                style={inverted ?
                    {
                        background: theme.colorSecondary,
                        color: theme.colorPrimary,
                        border: `2px solid ${theme.colorPrimary}`
                    }
                    :
                    {
                        background: theme.colorPrimary,
                        color: theme.colorSecondary,
                        border: `2px solid ${theme.colorPrimary}`
                    }}>
                {title}
            </button>
            <div className={expanded ? "childlink-tray" : "childlink-tray hidden"}>
                <AudioPlayer vendor={ currentProvider }/>
                <ul className="childlink-container audiochildlink-container">
                    {
                        audioChildLinks.map((link, i) => {
                            return (<AudioChildLink key={i} data={link} font={theme.colorSecondary} setCurrentProvider={ setCurrentProvider }/>)
                        })
                    }
                </ul>
            </div>
        </>
    )
}

export default LinkWithAudioPlayer