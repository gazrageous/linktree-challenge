import React, { useContext, useState } from 'react'
import { ThemeContext } from '../../contexts/ThemeContext'
import Arrow from '../../assets/icons/arrow.svg'
import SongKick from '../../assets/icons/by-songkick-wordmark.svg'

interface ChildLink {
    title: string,
    subtitle?: string,
    url: string,
    disabled?: boolean,
    disabledLabel?: string
}

interface LinkWithChildLinksProps {
    title: string,
    childLinks: ChildLink[]
}


const ChildLink = (props) => {
    const { title, subtitle, url, disabled, disabledLabel } = props.data

    return (
        <li>
            <a
                className="childlink"
                href={url}
                target="_blank"
                rel="noopener noreferrer"
            >
                <div>
                    <h4>{title}</h4>
                    <p className="subtitle">{subtitle}</p>
                </div>
                {disabled ? <p className="childlink-disabled">{disabledLabel}</p> : <img className="arrow" src={Arrow} />}
            </a>
        </li>
    )
}

const LinkWithChildLinks: React.FC<LinkWithChildLinksProps> = (props) => {

    const { title, childLinks } = props;
    const { theme }: any = useContext(ThemeContext)

    const [inverted, setInverted] = useState(false)
    const [expanded, setExpanded] = useState(false)

    return (
        <>
            <button
                className="link link-btn"
                onMouseEnter={() => setInverted(true)}
                onMouseLeave={() => setInverted(false)}
                onClick={() => setExpanded(!expanded)}
                style={inverted ?
                    {
                        background: theme.colorSecondary,
                        color: theme.colorPrimary,
                        border: `2px solid ${theme.colorPrimary}`
                    }
                    :
                    {
                        background: theme.colorPrimary,
                        color: theme.colorSecondary,
                        border: `2px solid ${theme.colorPrimary}`
                    }}>
                {title}
            </button>
            <div className={expanded ? "childlink-tray" : "childlink-tray hidden"}>
                <ul className="childlink-container">
                    {
                        childLinks.map((link, i) => <ChildLink key={i} data={link} font={theme.colorSecondary} />)
                    }
                </ul>
                <div className="childlink-provider-logo">
                    <img src={SongKick} alt="Songkick logo" />
                </div>
            </div>
        </>
    )
}

export default LinkWithChildLinks