// @todo - add in Types and TS where possible as this component - while a POC piece, could be turned around as the user settings components

import React, { useState, useContext } from 'react'
import { ProfileContext } from '../../contexts/ProfileContext';
import { ThemeContext } from '../../contexts/ThemeContext'
import Arrow from '../../assets/icons/arrow.svg'
import './TestWidget.css'

const TestWidget: React.FC = () => {

    const [title, setTitle] = useState('');
    const [url, setURL] = useState('');
    const [trayOpen, setTrayOpen] = useState(false)

    const { dispatch }: any = useContext(ProfileContext);
    const { theme, setThemeInContextAndStorage }: any = useContext(ThemeContext)

    const handleSubmit = (e) => {
        e.preventDefault();
        dispatch({ type: 'ADD_LINK', newLink: { title, url, type: 'classic_link' } })
        setTitle('');
        setURL('');
    }

    return (
        <div className="testwidget-container">
            <div className={trayOpen ? "testwidget-tag" : "testwidget-tag hidden"} onClick={() => setTrayOpen(!trayOpen)}>
                <h2>PoC Settings Tray <img src={Arrow} alt="arrow for settings tray" /></h2>
            </div>
            <div className="testwidget-inner">
                <form className="testwidget-themeform" >
                    <h3>Theme:</h3>
                    <label htmlFor="color-primary">Primary Color</label>
                    <input type="color" id="color-primary" value={theme.colorPrimary} onChange={(e) => setThemeInContextAndStorage({ ...theme, colorPrimary: e.target.value })} />

                    <label htmlFor="color-secondary">Secondary Color</label>
                    <input type="color" id="color-secondary" value={theme.colorSecondary} onChange={(e) => setThemeInContextAndStorage({ ...theme, colorSecondary: e.target.value })} />
                </form>
                <form className="testwidget-linkform" onSubmit={handleSubmit}>
                    <h3>New Classic Link Form:</h3>

                    <label htmlFor="link-title">Link Title</label>
                    <input type="text" id="link-title" value={title} onChange={(e) => setTitle(e.target.value)} />

                    <label htmlFor="link-url">Link URL</label>
                    <input type="text" id="link-url" value={url} onChange={(e) => setURL(e.target.value)} />

                    <input
                        disabled={!title || !url}
                        style={
                            {
                                background: theme.colorPrimary,
                                color: theme.colorSecondary,
                                border: `2px solid ${theme.colorPrimary}`
                            }}
                        type="submit"
                        value="Submit"
                    />
                </form>
            </div>
        </div>
    )
}

export default TestWidget