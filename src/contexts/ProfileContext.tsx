import React, { createContext, useReducer, useState, useEffect } from 'react';
import LinkReducer from '../reducers/LinkReducer';

export const ProfileContext = createContext({});

const fetchLinkState = fetch(`/api/request-profile/?username=${window.location.pathname.replace("/", "")}`)
    .then(res => res.json())
    .catch(e => {
        console.log('error encounterd when requesting link state from serverless in ProfileContext:', e)
        return {};
    })


const initialState = [];

const ProfileContextProvider: React.FC<{ children: any }> = ({ children }) => {
    const [links, dispatch] = useReducer(LinkReducer, initialState);
    const [profile, setProfile] = useState({
        name: "yourname",
        id: "0000"
    })

    useEffect(() => {
        const mounted = true
        fetchLinkState.then((data) => {
            mounted && dispatch({ type: 'INIT_LINKS', initArray: data.profile.links })
            setProfile(data.profile.userData)
        });
    }, [])

    return (
        <ProfileContext.Provider value={{ profile, links, dispatch }}>
            {children}
        </ProfileContext.Provider>
    );
}

export default ProfileContextProvider;