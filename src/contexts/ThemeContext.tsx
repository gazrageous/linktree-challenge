import React, { createContext, useState, useEffect } from 'react';

export const ThemeContext = createContext({});

const ThemeContextProvider: React.FC = ({ children }) => {

    const [theme, setTheme] = useState({
        colorPrimary: '#39E09B',
        colorSecondary: '#263238'
    });

    const setThemeInContextAndStorage = (newTheme) => {
        localStorage.setItem("userThemePreference", JSON.stringify(newTheme));
        setTheme(newTheme)
    }

    useEffect(() => {
        const prev = localStorage.getItem('userThemePreference')
        if (prev) setTheme(JSON.parse(prev))
    }, [])

    return (
        <ThemeContext.Provider value={{ theme, setThemeInContextAndStorage, setTheme }}>
            {children}
        </ThemeContext.Provider>
    );
}

export default ThemeContextProvider;
