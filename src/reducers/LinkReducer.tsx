// @todo add additional CRUD operations to reducer to liaise with serverless/ to manage links independently and in reference to user profiles

type Link = {
    title: string
    type: 'audio_link' | 'classic_link' | 'link_with_children'
    url: string
}

type Action = {
    type: 'INIT_LINKS' | 'ADD_LINK'
    initArray?: Link[]
    newLink?: Link
}

const LinkReducer = (state: Link[] | any, action: Action): any => {
    switch (action.type) {
    case 'INIT_LINKS':
        return action.initArray
    case 'ADD_LINK':
        return [ ...state, action.newLink ]
    default:
        return state;
    }
};

export default LinkReducer